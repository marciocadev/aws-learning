from aws_cdk import (
    core as cdk,
    aws_dynamodb as dynamodb,
    aws_lambda as _lambda,
    aws_apigateway as apigw
)


class SqsFifoStack(cdk.Stack):

    def __init__(self, scope: cdk.Construct, construct_id: str,
            table: dynamodb.Table, table_name: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        lambda_db = _lambda.Function(
            scope=self,
            id="sqs-fifo-db",
            function_name="sqs-fifo-db",
            handler="app.lambda_handler",
            code=_lambda.Code.asset("sqs-fifo/db"),
            runtime=_lambda.Runtime.PYTHON_3_8,
            environment={
                "TABLE_NAME": table_name
            }
        )
        table.grant_write_data(lambda_db)

