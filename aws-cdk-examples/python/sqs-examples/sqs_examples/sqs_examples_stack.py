from aws_cdk import (
    core as cdk,
    aws_dynamodb as dynamo
)


class SqsExamplesStack(cdk.Stack):

    def __init__(self, scope: cdk.Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        self.table_name = "sqs-example-db"
        self.table = dynamo.Table(
            scope=self,
            id="sqs-examples-db",
            table_name=self.table_name,
            partition_key=dynamo.Attribute(
                name="uuid",
                type=dynamo.AttributeType.STRING
            ),
            removal_policy=cdk.RemovalPolicy.DESTROY,
            billing_mode=dynamo.BillingMode.PAY_PER_REQUEST
        )


