import os
import boto3

db = boto3.resource("dynamodb")

def lambda_handler(event, context):

    table_name = os.environ.get("TABLE_NAME")
    table = db.Table(table_name)

    