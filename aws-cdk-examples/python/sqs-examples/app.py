#!/usr/bin/env python3
import os

from aws_cdk import core as cdk
from aws_cdk import core

from sqs_examples.sqs_examples_stack import SqsExamplesStack


app = core.App()
main_stack = SqsExamplesStack(app, "SqsExamplesStack")

app.synth()
