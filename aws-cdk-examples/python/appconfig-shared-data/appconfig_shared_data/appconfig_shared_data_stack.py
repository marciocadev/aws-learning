from aws_cdk import (
    core as cdk,
    aws_appconfig as appconfig
)


class AppconfigSharedDataStack(cdk.Stack):

    def __init__(self, scope: cdk.Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        # The code that defines your stack goes here
        
        application = appconfig.CfnApplication(
            scope=self,
            id="app-config-application",
            name="app-config-application",
            description="Sample AppConfig Application using CDK"
        )

        environment = appconfig.CfnEnvironment(
            scope=self,
            id="lambda-development-environment",
            name="app-config-sample-lambda-development-environment",
            application_id=application.ref,
            description="Sample AppConfig Development environment for Lambda implementation"
        )

        configuration_profile = appconfig.CfnConfigurationProfile(
            scope=self,
            id="configuration-profile",
            name="app-config-sample-configuration-profile",
            application_id=application.ref,
            location_uri="hosted",
            description="Sample AppConfig configuration profile"
        )

        hosted_configuration_profile = appconfig.CfnHostedConfigurationVersion(
            scope=self,
            id="hosted-configuration-profile",
            application_id=application.ref,
            configuration_profile_id=configuration_profile.ref,
            content_type="application/json",
            content='{\"boolEnableLimitResults\": true, \"intResultLimit\":5}',
            latest_version_number=1
        )

        hosted_configuration_profile.add_metadata("description", "Sample AppConfig hosted configuration profile content")

        deployment_strategy = appconfig.CfnDeploymentStrategy(
            scope=self,
            id="deployment-strategy",
            name="Custom.AllAtOnce",
            deployment_duration_in_minutes=0,
            growth_factor=100,
            final_bake_time_in_minutes=0,
            replicate_to="NONE",
            growth_type="LINEAR",
            description="Sample AppConfig deployment strategy - All at once deployment (i.e., immediate)"
        )

        deployment = appconfig.CfnDeployment(
            scope=self,
            id="Deployment",
            application_id=application.ref,
            configuration_profile_id=configuration_profile.ref,
            configuration_version="1",
            deployment_strategy_id=deployment_strategy.ref,
            environment_id=environment.ref
        )