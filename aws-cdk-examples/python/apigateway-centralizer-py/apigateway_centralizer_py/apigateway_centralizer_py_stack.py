from aws_cdk import (
    core as cdk,
    aws_apigateway as apigw
)


class ApigatewayCentralizerPyStack(cdk.Stack):

    def __init__(self, scope: cdk.Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        # Create base apigateway to all endpoint
        gateway = apigw.RestApi(
            scope=self,
            id="apigateway-centralizer-py",
            rest_api_name="apigateway-centralizer-py"
        )

        # Export apigateway id
        cdk.CfnOutput(
            scope=self,
            id="rest-api-id",
            export_name="rest-api-id",
            value=gateway.rest_api_id
        )

        # Create root resource
        mock_integration = apigw.MockIntegration()
        resource = apigw.Resource(
            scope=self,
            id="new-resource",
            parent=gateway.root,
            path_part=".",
            default_integration=mock_integration
        )
        resource.add_method(http_method="GET", integration=mock_integration)

        # Export root resource id
        cdk.CfnOutput(
            scope=self,
            id="root-resource-id",
            export_name="root-resource-id",
            value=resource.resource_id
        )