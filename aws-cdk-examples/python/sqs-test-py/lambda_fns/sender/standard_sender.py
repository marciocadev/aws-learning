import boto3
import os

sqs = boto3.client('sqs')

def handler(event, context):
    ''' Handler teste '''


    queue_url = os.environ.get("QUEUE_URL")

    response = sqs.send_message_batch(
        QueueUrl=queue_url,
        Entries=[
            {
                'Id': '1234',
                'MessageBody': 'Marcio',
                'DelaySeconds': 1
            },
            {
                'Id': '1244',
                'MessageBody': 'Marcio',
                'DelaySeconds': 1
            },
            {
                'Id': '1255',
                'MessageBody': 'Marcio',
                'DelaySeconds': 1
            }
        ]
    )
    
    response = sqs.send_message(
        QueueUrl=queue_url,
        MessageBody='Cruz',
        DelaySeconds=1
    )

    response = sqs.send_message(
        QueueUrl=queue_url,
        MessageBody='Cruz',
        DelaySeconds=1
    )

    response = sqs.send_message(
        QueueUrl=queue_url,
        MessageBody='Cruz',
        DelaySeconds=1
    )

    response = sqs.send_message(
        QueueUrl=queue_url,
        MessageBody='Almeida',
        DelaySeconds=1
    )

    queue_fifo_url = os.environ.get("QUEUE_FIFO_URL")

    response = sqs.send_message_batch(
        QueueUrl=queue_fifo_url,
        Entries=[
            {
                'Id': '1234',
                'MessageBody': 'Marcio',
                'MessageGroupId': '1'
            },
            {
                'Id': '1244',
                'MessageBody': 'Marcio',
                'MessageGroupId': '1'
            },
            {
                'Id': '1255',
                'MessageBody': 'Marcio',
                'MessageGroupId': '1'
            }
        ]
    )
    
    response = sqs.send_message(
        QueueUrl=queue_fifo_url,
        MessageBody='Cruz',
        MessageGroupId= '1'
    )

    response = sqs.send_message(
        QueueUrl=queue_fifo_url,
        MessageBody='Cruz',
        MessageGroupId= '1'
    )

    response = sqs.send_message(
        QueueUrl=queue_fifo_url,
        MessageBody='Cruz',
        MessageGroupId= '1'
    )

    response = sqs.send_message(
        QueueUrl=queue_fifo_url,
        MessageBody='Almeida',
        MessageGroupId= '1'
    )
#    queue_url = os.environ.get("QUEUE_URL")
#    
#    response = sqs.send_message(
#        QueueUrl=queue_url,
#        MessageBody='MARCIO CRUZ DE ALMEIDA',
#        DelaySeconds=123
#    )


