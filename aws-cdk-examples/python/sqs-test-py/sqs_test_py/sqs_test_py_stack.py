from aws_cdk import (
    core as cdk,
    aws_sqs as sqs,
    aws_lambda as _lambda,
    aws_lambda_event_sources as lambda_event,
)


class SqsTestPyStack(cdk.Stack):

    def __init__(self, scope: cdk.Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        sqs_standard = sqs.Queue(
            scope=self,
            id='standard-queue',
            queue_name='standard-queue',
            visibility_timeout=cdk.Duration.seconds(3),
            retention_period=cdk.Duration.seconds(300)
        )

        sqs_fifo = sqs.Queue(
            scope=self,
            id='fifo-queue',
            queue_name='sqstest.fifo',
            visibility_timeout=cdk.Duration.seconds(3),
            retention_period=cdk.Duration.seconds(300),
            fifo=True,
            content_based_deduplication=True
        )

        standard_lambda_sender = _lambda.Function(
            scope=self,
            id='standard-lambda-sender',
            function_name='standard-lambda-sender',
            runtime=_lambda.Runtime.PYTHON_3_8,
            handler='standard_sender.handler',
            code=_lambda.Code.from_asset('lambda_fns/sender'),
            environment={
                "QUEUE_URL": sqs_standard.queue_url,
                "QUEUE_FIFO_URL": sqs_fifo.queue_url
            }
        )

        standard_lambda_consumer = _lambda.Function(
            scope=self,
            id='standard-lambda-consumer',
            function_name='standard-lambda-consumer',
            runtime=_lambda.Runtime.PYTHON_3_8,
            handler='standard_consumer.handler',
            code=_lambda.Code.from_asset('lambda_fns/consumer')
        )

        sqs_standard.grant_send_messages(standard_lambda_sender)
        sqs_fifo.grant_send_messages(standard_lambda_sender)
        #sqs_standard.grant_consume_messages(standard_lambda_consumer)

        #standard_lambda_consumer.add_event_source(lambda_event.SqsEventSource(sqs_standard))

        