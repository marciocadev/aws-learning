from aws_cdk import (
    core as cdk,
    aws_apigateway as apigw,
    aws_sns as sns,
    aws_sns_subscriptions as sns_sub,
    aws_sqs as sqs,
    aws_lambda as _lambda,
    aws_lambda_event_sources as _event,
    aws_iam as iam
)

import json


class SnsSqsEventbridgeExamplePyStack(cdk.Stack):

    def __init__(self, scope: cdk.Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        base_apigateway_id = "base-apigateway-id"
        base_root_id = "base-root-id"

        topic = sns.Topic(
            scope=self,
            id="SNS-book-other-split",
            display_name="SNS-book-cd-split",
            topic_name="SNS-book-cd-split"
        )

        queue_book = sqs.Queue(
            scope=self,
            id="SQS-book",
            queue_name="SQS-book",
            visibility_timeout=cdk.Duration.seconds(300)
        )

        book_filter = sns.SubscriptionFilter.string_filter(
            whitelist=['book']
        )
        topic.add_subscription(
            sns_sub.SqsSubscription(
                queue=queue_book,
                raw_message_delivery=True,
                filter_policy={
                    'type': book_filter
                }
            )
        )

        other_filter = sns.SubscriptionFilter.string_filter(blacklist=['book'])
        queue_other = sqs.Queue(
            scope=self,
            id="SQS-other",
            queue_name="SQS-other",
            visibility_timeout=cdk.Duration.seconds(300)
        )
        topic.add_subscription(
            sns_sub.SqsSubscription(
                queue=queue_other,
                raw_message_delivery=True,
                filter_policy={
                    'type': other_filter
                }
            )
        )

        lambda_book = _lambda.Function(
            scope=self,
            id="lambda_book",
            runtime=_lambda.Runtime.PYTHON_3_8,
            handler="book.handler",
            code=_lambda.Code.from_asset("lambda_fns")
        )
        queue_book.grant_consume_messages(lambda_book)
        lambda_book.add_event_source(_event.SqsEventSource(queue_book))

        lambda_other = _lambda.Function(
            scope=self,
            id="lambda_other",
            runtime=_lambda.Runtime.PYTHON_3_8,
            handler="other.handler",
            code=_lambda.Code.from_asset("lambda_fns")
        )
        queue_other.grant_consume_messages(lambda_other)
        lambda_other.add_event_source(_event.SqsEventSource(queue_other))

        gateway = apigw.RestApi.from_rest_api_attributes(
            scope=self,
            id='split3',
            rest_api_id=cdk.Fn.import_value(base_apigateway_id),
            root_resource_id=cdk.Fn.import_value(base_root_id)
        )

        gw_sns_role = iam.Role(
            scope=self,
            id="GwTopícRole",
            assumed_by=iam.ServicePrincipal(
                service="apigateway.amazonaws.com"
            )
        )
        topic.grant_publish(gw_sns_role)

        response_model = apigw.Model(
            scope=self,
            id="ResponseModel",
            content_type="application/json",
            model_name="ResponseModel",
            rest_api=gateway,
            schema=apigw.JsonSchema(
                schema=apigw.JsonSchemaVersion.DRAFT4,
                title="ResponseModel",
                type=apigw.JsonSchemaType.OBJECT,
                properties={
                    'item': apigw.JsonSchema(type=apigw.JsonSchemaType.STRING)
                }
            )
        )

        error_response_model = apigw.Model(
            scope=self,
            id="ErrorResponseModel",
            content_type="application/json",
            model_name="ErrorResponseModel",
            rest_api=gateway,
            schema=apigw.JsonSchema(
                schema=apigw.JsonSchemaVersion.DRAFT4,
                title="ErrorResponseModel",
                type=apigw.JsonSchemaType.OBJECT,
                properties={
                    'type': apigw.JsonSchema(type=apigw.JsonSchemaType.STRING),
                    'item': apigw.JsonSchema(type=apigw.JsonSchemaType.STRING)
                }
            )
        )

        request_template = "Action=Publish&" + \
                           "TargetArn=$util.urlEncode('" + topic.topic_arn + "')&" + \
                           "Message=$util.urlEncode($input.path('$.item'))&" + \
                           "Version=2010-03-31&" + \
                           "MessageAttributes.entry.1.Name=type&" + \
                           "MessageAttributes.entry.1.Value.DataType=String&" + \
                           "MessageAttributes.entry.1.Value.StringValue=$util.urlEncode($input.path('$.type'))"

        error_template = {
            "state": 'error',
            "message": "$util.escapeJavaScript($input.path('$.errorMessage'))"
        }
        error_template_string = json.dumps(error_template, separators=(',', ':'))

        # This is how our gateway chooses what response to send based on selection_pattern
        integration_options = apigw.IntegrationOptions(
            credentials_role=gw_sns_role,
            request_parameters={
                'integration.request.header.Content-Type': "'application/x-www-form-urlencoded'"
            },
            request_templates={
                "application/json": request_template
            },
            passthrough_behavior=apigw.PassthroughBehavior.NEVER,
            integration_responses=[
                apigw.IntegrationResponse(
                    status_code='200',
                    response_templates={
                        "application/json": json.dumps({"item": 'message added to topic'})
                    }),
                apigw.IntegrationResponse(
                    selection_pattern="^\[Error\].*",
                    status_code='400',
                    response_templates={
                        "application/json": error_template_string
                    },
                    response_parameters={
                        'method.response.header.Content-Type': "'application/json'",
                        'method.response.header.Access-Control-Allow-Origin': "'*'",
                        'method.response.header.Access-Control-Allow-Credentials': "'true'"
                    }
                )
            ]
        )

        # Add an SendEvent endpoint onto the gateway
        gateway.root.add_resource('SendEvent') \
            .add_method('POST', apigw.Integration(type=apigw.IntegrationType.AWS,
                                                   integration_http_method='POST',
                                                   uri='arn:aws:apigateway:us-east-1:sns:path//',
                                                   options=integration_options
                                                   ),
                        method_responses=[
                            apigw.MethodResponse(status_code='200',
                                                  response_parameters={
                                                      'method.response.header.Content-Type': True,
                                                      'method.response.header.Access-Control-Allow-Origin': True,
                                                      'method.response.header.Access-Control-Allow-Credentials': True
                                                  },
                                                  response_models={
                                                      'application/json': response_model
                                                  }),
                            apigw.MethodResponse(status_code='400',
                                                  response_parameters={
                                                      'method.response.header.Content-Type': True,
                                                      'method.response.header.Access-Control-Allow-Origin': True,
                                                      'method.response.header.Access-Control-Allow-Credentials': True
                                                  },
                                                  response_models={
                                                      'application/json': error_response_model
                                                  }),
                        ]
                        )