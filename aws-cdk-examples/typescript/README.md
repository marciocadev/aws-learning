# aws-cdk-examples

### Comandos úteis

Crie o diretório onde o projeto será criado, entre no diretório e inicialize o projeto
```
mkdir <nome_do_projeto>
cd <nome_do_projeto>
cdk init app --language=typescript
```

Para fazer o deploy do projeto eu utilizo o comando abaixo
```
cdk deploy --profile <profile> --require-approval "never"
```
* --profile
  * Necessário somente se quiser especificar o profile desejado, caso seja o default não precisa especificart
* --require-approval
  * Faz com que não seja necessário confirmar cada criação de stack, ideal para projetos grandes


Para fazer a remoção do projeto eu utilizo o comando abaixo
```
cdk destroy --profile <profile> --require-approval "never"
```
* --profile
  * Necessário somente se quiser especificar o profile desejado, caso seja o default não precisa especificart
* --require-approval
  * Faz com que não seja necessário confirmar cada criação de stack, ideal para projetos grandes

### Dependências

Ao criar um projeto coloque as dependências no arquivo __package.json__ e execute os comandos abaixo para iniciar o projeto
```
npm install
```

[Voltar para os projetos com AWS CDK](../README.md)