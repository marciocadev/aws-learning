import * as cdk from '@aws-cdk/core';
import apigw = require('@aws-cdk/aws-apigateway');

export class ApigatewayCentralizerTsStack extends cdk.Stack {

  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    let gateway = new apigw.RestApi(
      this,
      "apigateway-centralizer-ts",
      { restApiName: "apigateway-centralizer-ts" }
    );

    gateway.root.addMethod(
      "GET",
      undefined, 
      { operationName: "root-method" }
    )

  }
}
