# Exemplos usando o CDK
* [aws-cdk-examples](./aws-cdk-examples/)

# AWS Command Line Interface
AWS Command Line Interface (CLI) é a ferramenta básica para gerenciar seus serviços na AWS

## Instalando o AWS-CLI

[Installing, updating, and uninstalling the AWS CLI version 2 on Linux](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-linux.html)

```
mkdir aws-cli-install && cd aws-cli-install
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
cd .. && rm -rf aws-cli-install
```

## Configuração Básica

Logue na conta root na aws e entre no console IAM 

Crie um novo usuário e de as permissões
* Programmatic Access 
* AWS Management Console access 

Ao final do processo você terá os dados necessários para configurar o AWS-CLI e será disponibilizado um arquivo CSV com estes dados


__Identity and Access Management (IAM)__

[Configuration basics](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html)

* aws configure

```
AWS Access Key ID [None]: ***
AWS Secret Access Key [None]: ***
Default region name [None]: us-east-1
Default output format [None]: json
```

A região us-east-1 é a mais barata neste momento (17/04/2021)

Isso gera os arquivos de configuração no diretório padrão da AWS (__~/.aws__)

Serão gerados 2 arquivos básicos, um de credencial e outro de config

E pode ser incluido novas credenciais e incluidas nos arquivos

Exemplo do arquivo credentials
```
[default]
aws_access_key_id = ***
aws_secret_access_key = ***

[teste]
aws_access_key_id = ***
aws_secret_access_key = ***
```

Exemplo do arquivo config
```
[default]
region = us-east-1
output = json

[profile teste]
region = us-east-1
output = json
```


# AWS Serverless Application Model
AWS Serverless Application Model (SAM) é um framework open-source para construir aplicativos serverless.

Basicamente utilizamos eles para rodar localmente nossos lambda e executar testes

## Instalando o AWS-SAM

[Installing, updating, and uninstalling the AWS CLI version 2 on Linux](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install-linux.html)

```
mkdir sam-install && cd sam-install
curl -L "https://github.com/aws/aws-sam-cli/releases/latest/download/aws-sam-cli-linux-x86_64.zip" -o sam.zip
unzip sam.zip
sudo ./install
cd .. && rm -rf sam-install
```

# AWS Cloud Development Kit (CDK)
AWS Cloud Development Kit (CDK) é um framework open-source para desenvolver sua arquitetura na cloud de forma programatica

## Instalando o AWS-CDK

```
npm install -g aws-cdk
```

# Serverless Framework
O Serverless Framework é um framework open-source para desenvolvimento de arquiteturas na cloud independente do serviço utilizado, desta forma ode ser utilizado na AWS, Azure, Google Cloud entre outros (passivel de customizações para o ambiente de destino)

## Instalando o Serverless Framework

```
npm install -g serverless
```